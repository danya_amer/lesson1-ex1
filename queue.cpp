#include<stdio.h>
#define QUEUE_H

typedef struct queue
{
	int* elements;
	int maxSize;
} queue;

void initQueue(queue* q, unsigned int size)
{
	q->elements = new int[size];
	q->maxSize = size;
}
void cleanQueue(queue* q)
{
	delete[] q->elements;
}
void enqueue(queue* q, unsigned int newValue)
{
	int i = 0;
	while (q->elements[i] != 0)
		i++;
	if (i < q->maxSize)
		q->elements[i] = newValue;
}
int dequeue(queue* q)
{
	int result = q->elements[0];
	for (int i = 0; i < q->maxSize - 1; i++)
	{
		q->elements[i] = q->elements[i + 1];
	}
	return result;
}